<?php

namespace App\Question\Tests\Domain\Question;

use App\Question\Domain\Question;
use App\Question\Domain\VO\CategoryId;
use App\Question\Domain\VO\Id;
use App\Question\Domain\VO\Number;
use App\Question\Domain\VO\Priority;
use App\Shared\Domain\Exception\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Uuid;

/**
 * @covers \App\Question\Domain\Question
 */
class CreateTest extends KernelTestCase
{
    public function testConstructWithValidQuestion(): void
    {
        $question = new Question(
            new Id(),
            new Number('1234'),
            new CategoryId(Uuid::v4()),
            Priority::medium,
            'This is a valid question',
        );

        $questionCreatedEvent = $question->popEvents()[0];
        $this->assertSame('This is a valid question', $questionCreatedEvent->question);
    }

    /**
     * @dataProvider invalidQuestionProvider
     */
    public function testQuestionThrowsExceptionWhenInvalidQuestionLength(string $question): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid question length');

        new Question(
            new Id(Uuid::v4()),
            new Number('1234'),
            new CategoryId(Uuid::v4()),
            Priority::medium,
            $question
        );
    }

    public function testChangeCategory(): void
    {
        $categoryId = new CategoryId(Uuid::v4());
        $question = new Question(
            new Id(),
            new Number('1234'),
            $categoryId,
            Priority::medium,
            'This is a valid question',
        );

        $events = $question->popEvents();
        $this->assertSame($categoryId, $events[0]->categoryId);

        $newCategoryId = new CategoryId(Uuid::v4());
        $question->changeCategory($newCategoryId);

        $events = $question->popEvents();
        $this->assertSame($newCategoryId, $events[0]->categoryId);
    }

    public function testChangePriority(): void
    {
        $question = new Question(
            new Id(),
            new Number('1234'),
            new CategoryId(Uuid::v4()),
            Priority::medium,
            'This is a valid question',
        );

        $events = $question->popEvents();
        $this->assertSame(Priority::medium, $events[0]->priority);

        $question->changePriority(Priority::high);

        $events = $question->popEvents();
        $this->assertSame(Priority::high, $events[0]->priority);
    }

    public function invalidQuestionProvider(): array
    {
        return [
            [''],
            [' '],
            [str_repeat('Length 10 ', 251)],
        ];
    }
}
