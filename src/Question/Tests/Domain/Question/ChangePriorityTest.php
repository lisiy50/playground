<?php

namespace App\Question\Tests\Domain\Question;

use App\Question\Domain\Question;
use App\Question\Domain\VO\CategoryId;
use App\Question\Domain\VO\Id;
use App\Question\Domain\VO\Number;
use App\Question\Domain\VO\Priority;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Uuid;

/**
 * @covers \App\Question\Domain\Question::changePriority
 */
class ChangePriorityTest extends KernelTestCase
{
    public function testChangePriority(): void
    {
        $question = new Question(
            new Id(),
            new Number('1234'),
            new CategoryId(Uuid::v4()),
            Priority::medium,
            'This is a valid question',
        );

        $events = $question->popEvents();
        $this->assertSame(Priority::medium, $events[0]->priority);

        $question->changePriority(Priority::high);

        $events = $question->popEvents();
        $this->assertSame(Priority::high, $events[0]->priority);
    }
}
