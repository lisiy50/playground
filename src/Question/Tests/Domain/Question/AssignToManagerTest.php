<?php

namespace App\Question\Tests\Domain\Question;

use App\Question\Domain\Question;
use App\Question\Domain\VO\CategoryId;
use App\Question\Domain\VO\Id;
use App\Question\Domain\VO\Number;
use App\Question\Domain\VO\Priority;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Uuid;

/**
 * @covers \App\Question\Domain\Question
 */
class AssignToManagerTest extends KernelTestCase
{
    /**
     * @covers \App\Question\Domain\Question::__construct
     */
    public function testAutoAssign(): void
    {
        // todo: auto assign to manager should be synchronous operation through event the constructor
        $this->markTestIncomplete();
    }

    /**
     * @covers \App\Question\Domain\Question::assign
     */
    public function testAssign(): void
    {
        $question = new Question(
            new Id(),
            new Number('1234'),
            new CategoryId(Uuid::v4()),
            Priority::medium,
            'This is a valid question',
        );

        $events = $question->popEvents();
        $this->assertSame(null, $events[0]->managerId);

        $managerId = new CategoryId(Uuid::v4());
        $question->assign($managerId);

        $events = $question->popEvents();
        $this->assertSame($managerId, $events[0]->managerId);
    }
}
