<?php

namespace App\Question\Tests\Domain\Question;

use App\Question\Domain\Question;
use App\Question\Domain\VO\CategoryId;
use App\Question\Domain\VO\Id;
use App\Question\Domain\VO\Number;
use App\Question\Domain\VO\Priority;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Uid\Uuid;

/**
 * @covers \App\Question\Domain\Question::changeCategory
 */
class ChangeCategoryTest extends KernelTestCase
{
    public function testChangeCategory(): void
    {
        $categoryId = new CategoryId(Uuid::v4());
        $question = new Question(
            new Id(),
            new Number('1234'),
            $categoryId,
            Priority::medium,
            'This is a valid question',
        );

        $events = $question->popEvents();
        $this->assertSame($categoryId, $events[0]->categoryId);

        $newCategoryId = new CategoryId(Uuid::v4());
        $question->changeCategory($newCategoryId);

        $events = $question->popEvents();
        $this->assertSame($newCategoryId, $events[0]->categoryId);
    }
}
