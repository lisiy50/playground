<?php

namespace App\Question\Tests\Domain\VO;

use App\Question\Domain\VO\Number;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @covers \App\Question\Domain\VO\Number
 */
class NumberTest extends KernelTestCase
{
    /**
     * @dataProvider provideInvalidValues
     */
    public function testThrowsExceptionOnInvalidValue(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid value provided');

        new Number('123');
    }

    public function testValueIsCorrectlySet(): void
    {
        $number = new Number('1234');

        $this->assertEquals('1234', $number->value);
    }

    public function provideInvalidValues(): array
    {
        return [
            [''],
            ['123'],
            ['12345'],
            ['abc'],
            ['123a'],
            ['a123'],
            ['123456'],
            ['12345a'],
            ['1234a'],
            ['a1234'],
            [' '],
        ];
    }
}
