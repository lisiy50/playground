<?php

namespace App\Question\UI\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'question:create',
    description: 'Add a short description for your command',
)]
class QuestionCreateCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->addArgument('question', InputArgument::REQUIRED, 'Question text.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $question = $input->getArgument('question');

        if ($question) {
            $io->note(sprintf('You passed an argument: %s', $question));
        }

        $io->success('Question have been created.');

        return Command::SUCCESS;
    }
}
