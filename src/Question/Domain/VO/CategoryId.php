<?php

declare(strict_types=1);

namespace App\Question\Domain\VO;

use Symfony\Component\Uid\UuidV4;

final class CategoryId extends UuidV4
{
    public function __construct(string $uuid)
    {
        parent::__construct($uuid);
    }
}
