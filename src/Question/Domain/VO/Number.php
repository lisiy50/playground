<?php

declare(strict_types=1);

namespace App\Question\Domain\VO;

final readonly class Number
{
    public string $value;

    public function __construct(string $value)
    {
        if (!preg_match('#^[1-9]\d{3}$#', $value)) {
            throw new \InvalidArgumentException('Invalid value provided');
        }
        $this->value = $value;
    }
}