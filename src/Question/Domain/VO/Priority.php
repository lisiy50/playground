<?php

declare(strict_types=1);

namespace App\Question\Domain\VO;

enum Priority: string
{
    case high = '10';
    case medium = '20';
    case low = '30';
}
