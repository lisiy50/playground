<?php

declare(strict_types=1);

namespace App\Question\Domain\Exception;

use App\Shared\Domain\Exception\DomainException;

final class InvalidQuestionValue extends \InvalidArgumentException implements DomainException
{
}
