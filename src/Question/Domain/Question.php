<?php

declare(strict_types=1);

namespace App\Question\Domain;

use App\Question\Domain\VO\CategoryId;
use App\Question\Domain\VO\Id;
use App\Question\Domain\VO\Number;
use App\Question\Domain\VO\Priority;
use App\Shared\Domain\AggregateRoot;
use App\Shared\Domain\Exception\InvalidArgumentException;

final class Question extends AggregateRoot
{
    private CategoryId $categoryId;
    private Priority $priority;
    private string $question;

    public function __construct(
        public readonly Id $questionId,
        public readonly Number $number,
        CategoryId $categoryId,
        Priority $priority,
        string $question,
    ) {
        $this->categoryId = $categoryId;
        $this->priority = $priority;
        $this->setQuestion($question);

        $this->emitEvent(new Event\QuestionCreated(
            $this->questionId,
            $this->number,
            $this->categoryId,
            $this->priority,
            $this->question,
        ));
    }

    public function changeCategory(CategoryId $categoryId): void
    {
        $this->categoryId = $categoryId;
        $this->emitEvent(new Event\CategoryChanged(
            $this->questionId,
            $this->categoryId,
        ));
    }

    public function changePriority(Priority $priority): void
    {
        $this->priority = $priority;
        $this->emitEvent(new Event\PriorityChanged(
            $this->questionId,
            $this->priority,
        ));
    }

    private function setQuestion(string $question): void
    {
        $trimmedQuestion = trim($question);
        $questionLength = mb_strlen($trimmedQuestion);
        if ($questionLength < 1 || $questionLength > 2500) {
            throw new InvalidArgumentException('Invalid question length');
        }
        $this->question = $trimmedQuestion;
    }
}
