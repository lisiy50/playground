<?php

declare(strict_types=1);

namespace App\Question\Domain\Event;

use App\Question\Domain\VO;
use App\Shared\Domain\Event;

final readonly class QuestionCreated extends Event
{
    public function __construct(
        public VO\Id $id,
        public VO\Number $number,
        public VO\CategoryId $categoryId,
        public VO\Priority $priority,
        public string $question,
    ) {
        parent::__construct();
    }
}