<?php

declare(strict_types=1);

namespace App\Question\Domain\Event;

use App\Question\Domain\VO;
use App\Shared\Domain\Event;

final readonly class CategoryChanged extends Event
{
    public function __construct(
        public VO\Id $questionId,
        public VO\CategoryId $categoryId,
    ) {
        parent::__construct();
    }
}