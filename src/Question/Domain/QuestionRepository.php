<?php

declare(strict_types=1);

namespace App\Question\Domain;

use App\Question\Domain\VO\Id;
use App\Question\Domain\VO\Number;

interface QuestionRepository
{
    public function nextNumber(): Number;

    public function findOne(Id $id): Question;

    public function save(Question $question): void;
}
