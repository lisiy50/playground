<?php

namespace App\Category\Tests;

use App\Category\Domain\Category;
use App\Category\Domain\Event\CategoryCreated;
use App\Category\Domain\Event\CategoryRenamed;
use App\Category\Domain\VO\Color;
use App\Category\Domain\VO\GroupId;
use App\Category\Domain\VO\Id;
use App\Shared\Domain\Exception\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @covers \App\Category\Domain\Category
 */
class CategoryTest extends KernelTestCase
{
    public function testCreate(): void
    {
        $category = new Category(new Id(), new GroupId(), 'name', Color::ENVIRONMENTAL);

        $this->assertSame('name', $category->getName());
        $this->assertSame(Color::ENVIRONMENTAL, $category->getColor());
    }

    /**
     * @dataProvider provideValidNames
     */
    public function testValidNames(string $name): void
    {
        $category = new Category(new Id(), new GroupId(), $name, Color::ENVIRONMENTAL);

        $this->assertSame(trim($name), $category->getName());
    }

    /**
     * @dataProvider provideInvalidNames
     */
    public function testInvalidNames(string $name): void
    {
        $this->expectException(InvalidArgumentException::class);

        new Category(new Id(), new GroupId(), $name, Color::ENVIRONMENTAL);
    }

    public function testTrimName(): void
    {
        $category = new Category(new Id(), new GroupId(), ' name ', Color::ENVIRONMENTAL);

        $this->assertSame('name', $category->getName());
    }

    public function testRename(): void
    {
        $category = new Category(new Id(), new GroupId(), 'name', Color::ENVIRONMENTAL);

        $category->rename('new name');

        $this->assertSame('new name', $category->getName());
    }

    public function testCreateEvent(): void
    {
        $category = new Category(new Id(), new GroupId(), 'name', Color::ENVIRONMENTAL);

        $events = $category->popEvents();
        $this->assertCount(1, $events);
        $this->assertInstanceOf(CategoryCreated::class, $events[0]);
    }

    public function testRenameEvent(): void
    {
        $category = new Category(new Id(), new GroupId(), 'name', Color::ENVIRONMENTAL);

        $category->rename('new name');

        $events = $category->popEvents();
        $this->assertCount(2, $events);
        $this->assertInstanceOf(CategoryCreated::class, $events[0]);
        $this->assertInstanceOf(CategoryRenamed::class, $events[1]);
    }

    public function provideValidNames(): array
    {
        return [
            ['n'],
            ['name with spaces'],
            [str_repeat('a', 50)],
            [' ' . str_repeat('a', 50) . ' '],
        ];
    }

    public function provideInvalidNames(): array
    {
        return [
            [''],
            [' '],
            [''],
            [str_repeat('a', 51)],
            ['a' . str_repeat(' ', 51) . 'a'],
        ];
    }
}
