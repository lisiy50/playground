<?php

declare(strict_types=1);

namespace App\Category\Domain;

use App\Category\Domain\Event\CategoryCreated;
use App\Category\Domain\Event\CategoryRenamed;
use App\Category\Domain\VO\Color;
use App\Category\Domain\VO\GroupId;
use App\Category\Domain\VO\Id;
use App\Shared\Domain\AggregateRoot;
use App\Shared\Domain\Exception\InvalidArgumentException;

final class Category extends AggregateRoot
{
    private string $name;

    public function __construct(
        public readonly Id $id,
        public readonly GroupId $groupId,
        string $name,
        public readonly Color $color
    ) {
        $this->setName($name);

        $this->emitEvent(new CategoryCreated($this->id, $this->groupId, $this->name, $this->color));
    }

    public function rename(string $name): void
    {
        $this->setName($name);

        $this->emitEvent(new CategoryRenamed($this->id, $this->name));
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getColor(): Color
    {
        return $this->color;
    }

    private function setName(string $name): void
    {
        $trimmedName = trim($name);
        $nameLength = mb_strlen($trimmedName);
        if ($nameLength < 1 || $nameLength > 50) {
            throw new InvalidArgumentException('Invalid name length');
        }
        $this->name = $trimmedName;
    }
}