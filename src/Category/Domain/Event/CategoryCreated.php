<?php

declare(strict_types=1);

namespace App\Category\Domain\Event;

use App\Category\Domain\VO;
use App\Shared\Domain\Event;

final readonly class CategoryCreated extends Event
{
    public function __construct(
        public VO\Id $id,
        public VO\GroupId $groupId,
        public string $name,
        public VO\Color $color
    ) {
        parent::__construct();
    }
}