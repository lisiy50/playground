<?php

declare(strict_types=1);

namespace App\Category\Domain\VO;

use Symfony\Component\Uid\UuidV4;

final class GroupId extends UuidV4
{
}
