<?php

declare(strict_types=1);

namespace App\Category\Domain\VO;

enum Color: string
{
    case ENVIRONMENTAL = '#CFE2EA';
    case FINANCE_ACCOUNTING = '#AAF3A3';
    case GENERAL_CORPORATE = '#FFC997';
    case HEALTH_SAFETY = '#B3D9F5';
    case HUMAN_RESOURCE = '#E6C7FF';
    case INFORMATION_TECHNOLOGY = '#98F9FF';
    case INSURANCE = '#FFCACA';
    case INTELLECTUAL_PROPERTY = '#D7F7A2';
    case LEGAL = '#E9CDC2';
    case MISC = '#DDDDDD';
    case OPERATIONS = '#CAD9FF';
    case PRODUCT = '#FFC1F9';
    case REAL_ESTATE = '#A6FFC4';
    case REGULATOR_COMPLIANCE = '#D7DDF9';
    case SALES_MARKETING = '#FFE484';
    case TAX = '#EED8B5';
    case TRANSACTIONAL_DOCUMENTS = '#DCE7B0';
}
