<?php

declare(strict_types=1);

namespace App\Scope\Domain\Event;

use App\Scope\Domain\VO\Id;
use App\Shared\Domain\Event;

final readonly class ScopeCreated extends Event
{
    public function __construct(
        public Id $scopeId,
        public string $name,
    ) {
        parent::__construct();
    }
}