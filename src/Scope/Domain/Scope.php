<?php

declare(strict_types=1);

namespace App\Scope\Domain;

use App\Scope\Domain\Event\ScopeCreated;
use App\Scope\Domain\Exception\InvalidScopeName;
use App\Scope\Domain\VO\Autoshare;
use App\Scope\Domain\VO\Id;
use App\Shared\Domain\AggregateRoot;

final class Scope extends AggregateRoot
{
    public readonly string $name;
    private Autoshare $autoshare;

    public function __construct(
        public readonly Id $scopeId,
        string $name,
    ) {
        $trimmedVal = trim($name);
        $trimmedValLength = mb_strlen($trimmedVal);
        if ($trimmedValLength < 2 || $trimmedValLength > 255) {
            throw new InvalidScopeName();
        }
        $this->name = $trimmedVal;
        $this->autoshare = new Autoshare();

        $this->emitEvent(new ScopeCreated($this->scopeId, $this->name));
    }

    public function enableAutoshareOnQuestionCreate(): void
    {
        $this->autoshare = new Autoshare(onQuestionCreate: true);
    }

    public function enableAutoshareOnQuestionReply(): void
    {
        $this->autoshare = new Autoshare(onQuestionReply: true);
    }

    public function disableAutoshare(): void
    {
        $this->autoshare = new Autoshare();
    }

    public function autoshareEnabled(): bool
    {
        return $this->autoshare->enabled;
    }

    public function autoshareOnQuestionCreate(): bool
    {
        return $this->autoshare->onQuestionCreate;
    }

    public function autoshareOnQuestionReply(): bool
    {
        return $this->autoshare->onQuestionReply;
    }
}