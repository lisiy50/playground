<?php

declare(strict_types=1);

namespace App\Scope\Domain\VO;

use Symfony\Component\Uid\UuidV4;

final class Id extends UuidV4
{
}
