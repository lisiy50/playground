<?php

declare(strict_types=1);

namespace App\Scope\Domain\VO;

use App\Shared\Domain\Exception\InvalidArgumentException;

final readonly class Autoshare
{
    public bool $enabled;

    public function __construct(
        public bool $onQuestionCreate = false,
        public bool $onQuestionReply = false,
    ) {
        if ($onQuestionCreate && $onQuestionReply) {
            throw new InvalidArgumentException('Autoshare cannot be enabled on both question create and reply');
        }
        $this->enabled = $onQuestionCreate || $onQuestionReply;
    }
}