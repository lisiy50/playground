<?php

declare(strict_types=1);

namespace App\Scope\Domain\Exception;

use App\Shared\Domain\Exception\DomainException;

final class InvalidScopeName extends \InvalidArgumentException implements DomainException
{
}
