<?php

namespace App\Scope\Tests\Domain;

use App\Scope\Domain\VO\Autoshare;
use App\Shared\Domain\Exception\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @covers \App\Scope\Domain\Scope
 */
class AutoshareTest extends KernelTestCase
{
    /**
     * @covers \App\Scope\Domain\Autoshare::__construct
     * @dataProvider provideValidParameters
     */
    public function testCreate(bool $onQuestionCreate, bool $onQuestionReply, bool $enabled): void
    {
        $autoshare = new Autoshare($onQuestionCreate, $onQuestionReply);

        $this->assertSame($enabled, $autoshare->enabled);
        $this->assertSame($onQuestionCreate, $autoshare->onQuestionCreate);
        $this->assertSame($onQuestionReply, $autoshare->onQuestionReply);
    }

    public function testBothShareOptionsCantBeEnabled(): void
    {
        $this->expectException(InvalidArgumentException::class);

        new Autoshare(true, true);
    }

    public function provideValidParameters(): array
    {
        return [
            [false, false, false],
            [true, false, true],
            [false, true, true],
        ];
    }
}
