<?php

namespace App\Scope\Tests\Domain;

use App\Scope\Domain\Exception\InvalidScopeName;
use App\Scope\Domain\Scope;
use App\Scope\Domain\VO\Id;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @covers \App\Scope\Domain\VO\Autoshare
 */
class ScopeTest extends KernelTestCase
{
    /**
     * @covers \App\Scope\Domain\Scope::__construct
     * @dataProvider provideValidNames
     */
    public function testConstructorNameValidation(string $name): void
    {
        $scope = new Scope(new Id(), $name);

        $this->assertSame($name, $scope->name);
    }

    /**
     * @covers \App\Scope\Domain\Scope::__construct
     */
    public function testConstructorNameTrim(): void
    {
        $scope = new Scope(new Id(), '  name  ');

        $this->assertSame('name', $scope->name);
    }

    /**
     * @covers \App\Scope\Domain\Scope::__construct
     */
    public function testConstructorNameLength(): void
    {
        $scope = new Scope(new Id(), str_repeat('a', 255));

        $this->assertSame(str_repeat('a', 255), $scope->name);
    }

    /**
     * @covers \App\Scope\Domain\Scope::__construct
     * @dataProvider provideInvalidNames
     */
    public function testConstructorNameLengthException(string $name): void
    {
        $this->expectException(InvalidScopeName::class);

        new Scope(new Id(), $name);
    }

    public function testEnableAutoshareOnQuestionCreate(): void
    {
        $scope = new Scope(new Id(), 'name');
        $scope->enableAutoshareOnQuestionCreate();

        $this->assertTrue($scope->autoshareEnabled());
        $this->assertTrue($scope->autoshareOnQuestionCreate());
        $this->assertFalse($scope->autoshareOnQuestionReply());
    }

    public function testEnableAutoshareOnQuestionReply(): void
    {
        $scope = new Scope(new Id(), 'name');
        $scope->enableAutoshareOnQuestionReply();

        $this->assertTrue($scope->autoshareEnabled());
        $this->assertFalse($scope->autoshareOnQuestionCreate());
        $this->assertTrue($scope->autoshareOnQuestionReply());
    }

    public function testDisableAutoshare(): void
    {
        $scope = new Scope(new Id(), 'name');
        $scope->enableAutoshareOnQuestionReply();
        $scope->disableAutoshare();

        $this->assertFalse($scope->autoshareEnabled());
        $this->assertFalse($scope->autoshareOnQuestionCreate());
        $this->assertFalse($scope->autoshareOnQuestionReply());
    }

    public function testEnableAutoshareOnQuestionCreateThenReply(): void
    {
        $scope = new Scope(new Id(), 'name');
        $scope->enableAutoshareOnQuestionCreate();
        $scope->enableAutoshareOnQuestionReply();

        $this->assertTrue($scope->autoshareEnabled());
        $this->assertFalse($scope->autoshareOnQuestionCreate());
        $this->assertTrue($scope->autoshareOnQuestionReply());
    }

    public function testEnableAutoshareOnQuestionReplyThenCreate(): void
    {
        $scope = new Scope(new Id(), 'name');
        $scope->enableAutoshareOnQuestionReply();
        $scope->enableAutoshareOnQuestionCreate();

        $this->assertTrue($scope->autoshareEnabled());
        $this->assertTrue($scope->autoshareOnQuestionCreate());
        $this->assertFalse($scope->autoshareOnQuestionReply());
    }

    public function provideValidNames(): array
    {
        return [
            ['aa'],
            [str_repeat('a', 255)],
        ];
    }

    public function provideInvalidNames(): array
    {
        return [
            [''],
            ['a'],
            [' a '],
            [str_repeat('a', 256)],
        ];
    }
}
