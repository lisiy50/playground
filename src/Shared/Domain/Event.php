<?php

declare(strict_types=1);

namespace App\Shared\Domain;

use DateTimeImmutable;

abstract readonly class Event
{
    public function __construct(
        public int $version = 1,
        public DateTimeImmutable $emittedAt = new DateTimeImmutable()
    ) {
    }
}
