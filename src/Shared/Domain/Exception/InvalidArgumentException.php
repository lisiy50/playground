<?php

declare(strict_types=1);

namespace App\Shared\Domain\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements DomainException
{
}