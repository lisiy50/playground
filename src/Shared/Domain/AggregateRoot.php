<?php

declare(strict_types=1);

namespace App\Shared\Domain;

abstract class AggregateRoot
{
    /** @var Event[] */
    private array $events = [];

    /**
     * @return Event[]
     */
    public function popEvents(): array
    {
        $events = $this->events;
        $this->events = [];

        return $events;
    }

    protected function emitEvent(Event $event): void
    {
        $this->events[] = $event;
    }
}
