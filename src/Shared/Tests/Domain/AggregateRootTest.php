<?php

namespace App\Shared\Tests\Domain;

use App\Shared\Domain\AggregateRoot;
use App\Shared\Domain\Event;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @covers \App\Shared\Domain\AggregateRoot
 */
class AggregateRootTest extends KernelTestCase
{
    public function testAggregateWithoutActions(): void
    {
        $aggregate = new class extends AggregateRoot {};

        $this->assertEmpty($aggregate->popEvents());
    }

    public function testEmitsEvents(): void
    {
        $event1 = new readonly class extends Event {};
        $event2 = new readonly class extends Event {};
        $evets = [$event1, $event2];
        $aggregate = new class($evets) extends AggregateRoot {
            public function __construct($events)
            {
                foreach ($events as $event)
                {
                    $this->emitEvent($event);
                }
            }
        };

        $events = $aggregate->popEvents();
        $this->assertCount(2, $events);
        $this->assertEquals($event1, $events[0]);
        $this->assertEquals($event2, $events[1]);
    }

    public function testAggregatePopEvents(): void
    {
        $event = new readonly class extends Event {};
        $aggregate = new class($event) extends AggregateRoot {
            public function __construct($event)
            {
                $this->emitEvent($event);
            }
        };

        $this->assertCount(1, $aggregate->popEvents());
        $this->assertCount(0, $aggregate->popEvents());
    }
}
